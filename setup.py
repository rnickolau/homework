# -*- coding: utf-8 -*-

from setuptools import setup
setup(
    name = 'homework',
    version = '0.1.0',
    packages = ['homework'],
    entry_points = {
        'console_scripts': [
            'homework = homework.__main__:main'
        ]
    })
