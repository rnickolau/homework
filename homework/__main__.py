# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 18:46:40 2017

@author: rynickol
"""

def calcDistance (dlat1, dlong1, dlat2, dlong2):
    from math import sin, cos, sqrt, atan2, radians 
    lat1 = radians(dlat1)
    long1 = radians(dlong1)
    lat2 = radians(dlat2)
    long2 = radians(dlong2)

    R = 6373
    a = sin((lat2-lat1) / 2)**2 + cos(lat1) * cos(lat2) * sin((long2-long1) / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return c*R

def main(dir="temp"):
    print('in main')
    
import pyrebase
import zipfile
import pandas as pd
import numpy as np
import openpyxl
import re, io
import sys, glob, sqlite3
import os
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
      
    #  Download the required file(s) from Firebase
    config = {
      "apiKey": "AIzaSyCUXQ-RDCRGzxTJ8DJwKc5VHhAMYAv6Kc4",
      "authDomain": "aoc-evo-178814.firebaseapp.com",
      "databaseURL": "https://aoc-evo-178814.firebaseio.com",
      "projectId": "aoc-evo-178814",
      "storageBucket": "aoc-evo-178814.appspot.com",
      "serviceAccount": "aoc-evo-178814-firebase-adminsdk-64yys-ecbb40a7a3.json"
    }
    firebase = pyrebase.initialize_app(config)
    auth = firebase.auth()
    user = auth.sign_in_with_email_and_password("aocevo@aoc.com", "a0c3v0")
    storage = firebase.storage()
    dataset_url = storage.child("AoCBootcampDataset2.zip").get_url(user['idToken'])
    storage.child("AoCBootcampDataset2.zip").download("AoCBootcampDataset2.zip")
    dataset_url = storage.child("AoCBootcampDataset.zip").get_url(user['idToken'])
    storage.child("AoCBootcampDataset.zip").download("AoCBootcampDataset.zip")    
    
    # Unzip the files
    zip_ref = zipfile.ZipFile("AoCBootcampDataset2.zip", 'r')
    zip_ref.extractall()
    zip_ref.close()
    with zipfile.ZipFile("AoCBootcampDataset.zip", 'r') as zfile:
        for name in zfile.namelist():
            if re.search(r'\.zip$', name) != None:
                zfiledata = io.BytesIO(zfile.read(name))
                with zipfile.ZipFile(zfiledata) as zfile2:
                    zfile2.extractall()
    
    # Open the excel workbook and load it into a dataframe
    df = pd.read_excel('T2 Site List.xlsx',sheetname='LTE Config (22)')
    
    # Scan the sites to check and see if there are any potential PCI conflicts in the 1900 band
    df1900 = df[df['ECELL_BAND_NUM']==1900]
    df1900 = df1900.dropna(subset=['ECELL_LATITUDE', 'ECELL_LONGITUDE', 'eNum', 'PCI'],)
    worst = 99
    for index, row in df1900.iterrows():
        lat1 = row['ECELL_LATITUDE']
        long1 = row['ECELL_LONGITUDE']
        site1 = row['eNum']
        pci1 = row['PCI']
        for index2, row2 in df1900.iterrows():
            lat2 = row2['ECELL_LATITUDE']
            long2 = row2['ECELL_LONGITUDE']
            site2 = row2['eNum']
            pci2 = row2['PCI']
            if site1 != site2:
                if pci1 == pci2:
                    distance = calcDistance(lat1, long1, lat2, long2)
                    if distance < 5:
                        eCell = row['ECELL_NAME']
                        print ('Possible PCI conflict -  Site 1:', 
                               site1, 'Site 2:', site2, 'Common 1900 PCI:', 
                               pci1, 'Distance:', distance, 'eCell:', eCell)                        
                        if distance < worst and eCell[:3] != "LBP" and eCell[:3] != "LBI":
                            worst = distance
                            worstCell = eCell
                            
    print (worstCell)                        
    
    
# since worst cell isn't in the other dataset, lets just graph another example
df2 = pd.read_excel('DropCall_Band2_Query_Result_LBHON_Nov25_Dec1.xlsx',sheetname='Subreport 1')
df2Sample = df2.loc[df2['Cell Name'] == 'LBHON0540811-044-1900-1-4']
df2Sample['Drop Rate']=df2Sample['L.E-RAB.AbnormRel.eNBTot'] / df2Sample['L.E-RAB.NormRel']
print(df2Sample)

# Create a trace
trace = go.Scatter(
    x = df2Sample['Drop Rate'],
    y = df2Sample['Time']
)

data = [trace]

#not working
#py.iplot(data)
        
    
if __name__ == '__main__':
    main()
    
    